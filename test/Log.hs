{-# OPTIONS_GHC -Wall #-}
module Log where

import L02.LogAnalysis
import L02.Log
import Test.QuickCheck
import Control.Monad

instance Arbitrary MessageType where
  arbitrary = oneof [pure Info, pure Warning, fmap Error arbitrary]

instance Arbitrary LogMessage where
  arbitrary = frequency [ (1, Unknown <$> arbitrary)
                        , (10, liftM3 LogMessage arbitrary arbitrary arbitrary)]

instance Arbitrary MessageTree where
  arbitrary = sized tree
    where tree 0 = pure Leaf
          tree n = oneof [pure Leaf, liftM3 Node subtree arbitrary subtree]
            where subtree = tree (n `div` 2)

areMessagesSorted :: [LogMessage] -> Bool
areMessagesSorted [] = True
areMessagesSorted [_] = True
areMessagesSorted (LogMessage _ tsx _ : LogMessage _ tsy _ : xs) = tsx <= tsy && areMessagesSorted xs
areMessagesSorted (_:xs) = areMessagesSorted xs

propTreeOrdered :: [LogMessage] -> Bool
propTreeOrdered = areMessagesSorted . inOrder . build

messageTreeOrderingWorks :: IO ()
messageTreeOrderingWorks = quickCheck propTreeOrdered
