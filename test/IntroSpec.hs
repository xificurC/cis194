module IntroSpec where

import Test.QuickCheck
import Test.QuickCheck.Arbitrary()
import L01.Intro

posInteger :: Gen Integer
posInteger = (arbitrary :: Gen Integer) `suchThat` (> 0)

toDigitsPositiveWorks :: IO ()
toDigitsPositiveWorks = quickCheck (forAll posInteger (\i -> show i == digitsToString (toDigits i)))

toDigitsNonPositiveWorks :: IO ()
toDigitsNonPositiveWorks = quickCheck (forAll arbitrarySizedNatural (\i -> toDigits (-i) == []))
