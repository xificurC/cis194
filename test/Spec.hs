import IntroSpec
import Log

main :: IO ()
main = do
  putStrLn ""
  toDigitsPositiveWorks
  toDigitsNonPositiveWorks
  messageTreeOrderingWorks
