{-# OPTIONS_GHC -Wall #-}
module L02.LogAnalysis where

import L02.Log
import Text.Read

parseMessage :: String -> LogMessage
parseMessage = parseMessageList . words

parseMessageList :: [String] -> LogMessage
parseMessageList ("I" : timestamp : message) =
  let msg = unwords message in
    case parseTimeStamp timestamp of
      Just ts -> LogMessage Info ts msg
      Nothing -> Unknown msg
parseMessageList ("W" : timestamp : message) =
  let msg = unwords message in
    case parseTimeStamp timestamp of
      Just ts -> LogMessage Warning ts msg
      Nothing -> Unknown msg
parseMessageList ("E" : errnum : timestamp : message) =
  let msg = unwords message in
    case parseErrNum errnum of
      Just err -> case parseTimeStamp timestamp of
        Just ts -> LogMessage (Error err) ts msg
        Nothing -> Unknown msg
      Nothing -> Unknown msg
parseMessageList s = Unknown (unwords s)

parseTimeStamp :: String -> Maybe TimeStamp
parseTimeStamp = readMaybe

parseErrNum :: String -> Maybe Int
parseErrNum = readMaybe

parse :: String -> [LogMessage]
parse = map parseMessage . lines

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert message Leaf = Node Leaf message Leaf
insert newMessage@(LogMessage _ newTimestamp _) (Node left message@(LogMessage _ timestamp _) right)
  | newTimestamp > timestamp = Node (insert newMessage left) message right
  | otherwise                = Node left message (insert newMessage right)
insert LogMessage{} (Node _ (Unknown _) _ ) = error "bogus"

build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left message right) = inOrder right ++ [message] ++ inOrder left

over50 :: LogMessage -> Bool
over50 (LogMessage (Error severity) _ _) | severity >= 50 = True
over50 _ = False

theMessage :: LogMessage -> String
theMessage (LogMessage _ _ msg) = msg
theMessage _ = ""

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map theMessage . filter over50 . inOrder . build
