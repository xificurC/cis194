{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module L12.Risk where

import Control.Monad.Random
import Data.List
import Data.Ord
-- import Debug.Trace

------------------------------------------------------------
-- Die values

newtype DieValue = DV { unDV :: Int }
  deriving (Eq, Ord, Show, Num)

first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

instance Random DieValue where
  random           = first DV . randomR (1,6)
  randomR (low,hi) = first DV . randomR (max 1 (unDV low), min 6 (unDV hi))

die :: Rand StdGen DieValue
die = getRandom

------------------------------------------------------------
-- Risk

type Army = Int

data Battlefield = Battlefield { attackers :: Army, defenders :: Army }
  deriving (Eq, Show)

battle :: Battlefield -> Rand StdGen Battlefield
battle bf@(Battlefield attack defend) = runFight bf maxAttack maxDefend where
  maxAttack = min 3 (attack - 1)
  maxDefend = min 2 defend

calculateFight :: Battlefield -> DieValue -> DieValue -> Battlefield
calculateFight (Battlefield attack defend) (DV attackDie) (DV defendDie)
  | attackDie > defendDie = Battlefield attack      (defend - 1)
  | otherwise             = Battlefield (attack -1) defend

calculateFights :: Battlefield -> [DieValue] -> [DieValue] -> Battlefield
calculateFights bf (a:as) (d:ds) = calculateFights (calculateFight bf a d) as ds
calculateFights bf _      _      = bf

runFight :: Battlefield -> Army -> Army -> Rand StdGen Battlefield
runFight bf attack defend = do
  attackDice <- sortBy (comparing Down) <$> replicateM attack die
  defendDice <- sortBy (comparing Down) <$> replicateM defend die
  pure $ calculateFights bf attackDice defendDice

invade :: Battlefield -> Rand StdGen Battlefield
invade bf@(Battlefield attack defend)
  | attack < 2 || defend == 0 = pure bf
  | otherwise                 = battle bf >>= invade

attackerWon :: Battlefield -> Bool
attackerWon (Battlefield _ d) = d == 0

successProb :: Battlefield -> Rand StdGen Double
successProb bf = ((/1000.0) . fromIntegral . length . filter id . map attackerWon) <$> replicateM 1000 (invade bf)
