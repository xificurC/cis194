module Golf where

everyNth :: [a] -> Int -> [a]
everyNth [] _ = []
everyNth x n = case drop (n-1) x of
  [] -> []
  (y:ys) -> y : everyNth ys n

skips :: [a] -> [[a]]
skips x = map (everyNth x) [1..(length x)]

localMaxima :: [Integer] -> [Integer]
localMaxima (a:b:c:xs) | b > a && b > c = b : localMaxima (c:xs)
                       | otherwise      = localMaxima (c:xs)
localMaxima _ = []
