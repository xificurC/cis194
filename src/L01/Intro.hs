module L01.Intro where

import Data.Char
-- import Test.QuickCheck
-- import Test.QuickCheck.Arbitrary

hailstone :: Integer -> Integer
hailstone n
  | n `mod` 2 == 0 = n `div` 2
  | otherwise      = 3*n + 1

hailstoneSeq :: Integer -> [Integer]
hailstoneSeq 1 = [1]
hailstoneSeq n = n : hailstoneSeq (hailstone n)

toDigitsRev :: Integer -> [Integer]
toDigitsRev n
  | n <= 0    = []
  | otherwise = n `mod` 10 : toDigitsRev (n `div` 10)

toDigits :: Integer -> [Integer]
toDigits = reverse . toDigitsRev

digitsToString :: [Integer] -> String
digitsToString = map (intToDigit . fromInteger)

doubleSecond :: [Integer] -> [Integer]
doubleSecond [] = []
doubleSecond [n] = [n]
doubleSecond (a:b:rest) = a : 2*b : doubleSecond rest

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther = reverse . doubleSecond . reverse

sumDigits :: [Integer] -> Integer
sumDigits = sum . concat . (map toDigits)

validate :: Integer -> Bool
validate = (0 ==) . (`rem` 10) . sumDigits . doubleEveryOther . toDigits


type Peg = String
type Move = (Peg, Peg)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 0 _ _ _ = []
hanoi n a b c = (hanoi (n-1) a c b) ++ (a, b) : hanoi (n-1) c b a
