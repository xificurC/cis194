module High where

fun1 :: [Integer] -> Integer
fun1 = product . map (subtract 2) . filter even

fun2 :: Integer -> Integer
fun2 = sum . takeWhile (/=0) . iterate it
  where it n | even n    = n `div` 2
             | otherwise = 3*n + 1

data Tree a = Leaf
            | Node Integer (Tree a) a (Tree a)
            deriving (Show, Eq)

foldTree :: [a] -> Tree a
foldTree = foldr f Leaf
  where f new Leaf                 = Node 0 Leaf new Leaf
        f new (Node h Leaf x Leaf) = Node (h+1) (Node 0 Leaf new Leaf) x Leaf
        f new (Node h Leaf x r)    = Node h (Node 0 Leaf new Leaf) x r
        f new (Node h l    x Leaf) = Node h l x (Node 0 Leaf new Leaf)
        f new (Node h l@(Node hl _ _ _) x r@(Node hr _ _ _))
          | hl < hr  = Node h (f new l) x r
          | hl > hr  = Node h l x (f new r)
          | hl == hr = Node (h+1) (f new l) x r
